import Vue from 'vue'
import Vuex from 'vuex'
/* global axios:true */

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    status: '',
    token: localStorage.getItem('token') || '',
    user: localStorage.getItem('user') || '',
    active: '',
    users: (localStorage.getItem('all') !== null ? localStorage.getItem('all').split(',') : [])
  },
  mutations: {
    auth_request (state) {
      state.status = 'loading'
    },
    auth_success (state, user) {
      state.status = 'success'
      state.token = user.token
      state.user = user.id
    },
    auth_error (state) {
      state.status = 'error'
    },
    logout (state) {
      state.status = ''
      state.token = ''
      state.user = ''
      state.users = []
    },
    set_active_user (state, id) {
      state.active = id
    },
    set_all_users (state, all) {
      all.map(item => state.users.push(item))
    }
  },
  actions: {
    logout ({ commit }) {
      localStorage.clear()
      commit('logout')
    },

    addUserToLocal ({ commit }, user) {
      this.getters.acceptedUsers.push(user.toString())
      localStorage.setItem('all', this.getters.acceptedUsers)
      commit('set_active_user', user)
    },

    storeAccData ({ commit }, user) {
      return new Promise(function (resolve, reject) {
        const token = generateToken()
        user.token = token
        axios
          .post('/accounts', user)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },

    storeSettings ({ commit }, user) {
      return new Promise((resolve, reject) => {
        axios
          .patch('/users/' + this.getters.activeUser, user)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => reject(error))
      })
    },

    storeUser ({ commit }, user) {
      return new Promise(function (resolve, reject) {
        axios
          .post('/users/', user)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },

    getUsers ({ commit }) {
      return new Promise((resolve, reject) => {
        axios
          .get('/users?account_id=' + this.getters.loggedId)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error)
          })
      })
    },

    getCards ({ commit }) {
      return new Promise((resolve, reject) => {
        axios
          .get('https://api.publicapis.org/entries?category=test&auth=null')
          .then(response => resolve(response.data.entries))
          .catch(error => reject(error))
      })
    },

    getSettings ({ commit }) {
      return new Promise((resolve, reject) => {
        axios
          .get('/users/' + this.getters.activeUser)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },

    getAccUsers ({ commit }) {
      return new Promise(function (resolve, reject) {
        axios
          .get('/users?account_id=' + localStorage.getItem('user'))
          .then(response => {
            let ids = []
            response.data.map(user => { ids.push(user.id) })
            localStorage.setItem('all', ids)
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },

    getAccData ({ commit }) {
      return new Promise(function (resolve, reject) {
        axios
          .get('/accounts')
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },

    getSelectedCard ({ commit }, key) {
      return new Promise(function (resolve, reject) {
        axios
          .get('https://api.publicapis.org/entries?category=test&auth=null&title=' + key)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },

    getApiCard ({ commit }, link) {
      return new Promise(function (resolve, reject) {
        axios
          .get(link)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    }
  },
  getters: {
    isLoggedIn: state => !!state.token,
    token: state => state.token,
    authStatus: state => state.status,
    activeUser: state => state.active,
    loggedId: state => state.user,
    acceptedUsers: state => state.users,
    correctAuth: state => state.correct_auth
  }
})

function generateToken () {
  return Math.random().toString(36).substring(2) + Math.random().toString(36).substring(2)
}
