import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/Login.vue'
import Register from './views/Register.vue'
import Dashboard from './views/Dashboard.vue'
import CardView from './components/CardView.vue'
import Settings from './views/Settings.vue'
import AddUser from './components/AddUser.vue'
import store from './store'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/',
      redirect: '/dashboard',
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/dashboard/:id',
      name: 'dashboard_user',
      component: Dashboard,
      meta: {
        requiresAuth: true
      },
      props: true
    },
    {
      path: '/dashboard/:id/:key_link',
      name: 'dashboard_user_card',
      component: CardView,
      meta: {
        requiresAuth: true
      },
      props: true
    },
    {
      path: '/settings',
      name: 'settings',
      component: Settings,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/settings/:id',
      name: 'settings_user',
      component: Settings,
      meta: {
        requiresAuth: true
      },
      props: true
    },
    {
      path: '/add',
      name: 'add_user',
      component: AddUser,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '*',
      redirect: '/dashboard',
      meta: {
        requiresAuth: true
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (store.getters.isLoggedIn) {
    if (store.getters.token) {
      if (to.path === '/' || to.path === '/login' || to.path === '/register') {
        if (store.getters.activeUser !== '' && store.getters.activeUser !== undefined) {
          next({ path: '/dashboard/' + store.getters.activeUser })
        } else { next({ path: '/dashboard' }) }
      } else if (to.path === '/settings' || to.path === '/settings/') {
        if (store.getters.activeUser !== '' && store.getters.activeUser !== undefined) {
          next({ path: '/settings/' + store.getters.activeUser })
        } else { next() }
      } else {
        let id = to.path.split('/')[2]
        if (store.getters.acceptedUsers.indexOf(id) !== -1) {
          store.commit('set_active_user', id).then(next())
        } else {
          next()
        }
      }
    } else {
      store.dispatch('logout')
      next({ path: '/login' })
    }
  } else {
    if (to.matched.some(record => record.meta.requiresAuth)) {
      next({ path: '/login' })
    }
    next()
  }
})

export default router
