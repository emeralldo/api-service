import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/element.js'

import Axios from 'axios'
Vue.config.productionTip = false
const axios = Axios.create({
  baseURL: `http://localhost:3000`
})
window.axios = axios

Vue.prototype.$baseUrl = 'http://localhost:3000'

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
