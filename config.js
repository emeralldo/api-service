module.exports = {
  baseUrl: process.env.NODE_ENV === 'production' ? '/dist/' : 'http://localhost:3000'
}
